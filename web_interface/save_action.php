<?php
  session_start();
  $search_keyword = str_replace(" ", "%20", $_GET["search_keyword"]);
//  echo "Session : ".$_SESSION['search_keyword']."<br>";
//  echo "Local : ".$search_keyword."<br>";
  if(!isset($_SESSION['search_keyword'])){
  	$_SESSION['search_keyword']="";
  }
  if(strcmp($_SESSION['search_keyword'], $search_keyword) == 0){ // search keyword matches. Ongoing session.
//    echo "Current session ".$_SESSION['search_keyword'];
  }
  else{	// does not match. New search session.
//  	echo "New session. ".$_SESSION['search_keyword'];
    $_SESSION['jsonValue'] = "";
  }

  
/*  if(!isset($_SESSION['search_keyword'])){
   echo "session was not set";
    $_SESSION['clicked_doc'] = "";
    $_SESSION['search_keyword'] = "";
    $_SESSION['jsonValue'] = "";
    $_SESSION['clicked_docs'] = array();
    $_SESSION['displayed_docs'] = array();
   echo "session set to ".$_SESSION['search_keyword'];
  }
  else{
    $_SESSION['clicked_docs'] = array();
    $_SESSION['displayed_docs'] = array();
    $_SESSION['search_keyword'] = "";
    $_SESSION['jsonValue'] = "";
 	 session_destroy();		//destroy session when new search triggered

 	echo "session set to ".$_SESSION['search_keyword'];
  }*/
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<link href="css/style.css" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Comfortaa" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Open+Sans+Condensed:300" rel="stylesheet">

	<link rel='stylesheet' type='text/css' href='//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css'>

	<title>Search Result</title>
</head>
<style type="text/css">/*
   table { page-break-inside:auto }
   tr    { page-break-inside:avoid; page-break-after:auto }*/

</style>
<body>
	<div class="search_bar_top_header">
		<div class="search_bar_top">
			<form class="form-horizontal" role="form" action='save_action.php' method='get'>
				<div class="row">						
					<div class="col-lg-6 banner_label"><h4><a href="index.php">HCDM Scientific Literature Search Engine </a></h4></div>
<!--					<div class="col-lg-4">
 						<label class= "control-label col-sm-6 blue_label" for="num_pages"># of pages to crawl: </label>
						<div class="input-group spinner ">
							<input type="text" name="num_pages" class="form-control" value="1">
							<div class="input-group-btn-vertical">
								<button class="btn btn-default" type="button"><i class="fa fa-caret-up"></i></button>
								<button class="btn btn-default" type="button"><i class="fa fa-caret-down"></i></button>
							</div>
						</div> 
					</div>-->
					<div class="col-lg-6 search_bar_top_button">
						<div class="input-group">
							<input type="text" class="form-control search_bar_save" name='search_keyword' placeholder="Search for..." value="<?php echo $_GET["search_keyword"];?>"
							>
              				<input size = 100 type="text" name='page' hidden="true" value="1">
							<span class="input-group-btn">
								<button type="submit" class="btn btn-primary search_button_top" type="button">Search</button>
<!-- 								<a href="Logout.php" class="btn btn-warning" role="button" aria-pressed="true">Logout</a>
 -->							</span>
						</div>
					</div>
					<!-- javascript:sendRequest($_SESSION['displayed_docs'],$_SESSION['clicked_docs']) -->
				</div>
			</form>

		</div>
	</div>

	<div class="containter table_background">
		<div id="loading_content" align="center">
		    <div id="error_msg" class="loading_message" style="margin-top: 50px">
		    	<h4>Loading may take few minutes if new documnet is being crawled and indexed.</h4>
		    </div>
		    <div id="loading_image" style="margin-top: 40px">
		        <!-- show loading image when opening the page -->
		        <img src="images/spinner.gif"/>
		    </div>
	
		</div>
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
	    <script type="text/javascript">
	       // your script to load content from php goes here
	       $(document).ready(function(){
			    $.ajax({        // call php script
			        url: 'loadGrid.php?search_keyword='+'<?php echo $search_keyword ?>' + '&page='+'<?php echo $_GET["page"] ?>',
			        type:'GET',
			        timeout: 30000,
			        contentType: 'html',
			        success: function (data) { 
			            // remove loading image and add content received from php 
			       		$('div#loading_content').html(data);

				    },
				    error: function(jqXHR, textStatus, errorThrown){
				            // in case something went wrong, show error
				        $('div#error_msg').append('Sorry, something went wrong: ' + textStatus + ' (' + errorThrown + ')');
				    }
				});
			});



	    </script>



	</div>
	<center>
		<footer>
			<p>&copy; 2017 <a href='http://www.cs.virginia.edu/~hw5x/'>Prof. Hongning Wang</a> - <a href='http://www.cs.virginia.edu'>Department of Computer Science</a> - <a href='http://www.virginia.edu'>University of Virginia</a></p>
		</footer>
	</center>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
	<!-- Latest compiled and minified JavaScript -->
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
	<script src="js/readMore.js"></script>
	<script src="js/sendRequest.js"></script>

	<!-- <script src="js/ui.js"></script> -->
</body>
</html>
