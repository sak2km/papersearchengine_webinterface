<?php
  session_start();
		include 'save_search_log.php';
		error_reporting(E_ALL);
		

	    $clicked_docs = array();
	    $displayed_docs = array();
	    $displayed_docs_lucene = array();
		$search_keyword = str_replace(" ", "%20", $_GET["search_keyword"]);
		$page = str_replace(" ", "%20", $_GET["page"]);
		$perPage = 10;
		$start = ($page-1)*$perPage;

  		$userId= $_SERVER['REMOTE_ADDR'];
		$date = date('y-m-d h:i:s');

		if(strcmp($_SESSION['search_keyword'], $search_keyword) == 0){ // search keyword matches. Ongoing session.
//			echo "search keyword matches. Ongoing session.";
			$json = $_SESSION['jsonValue'];
		}
		else{	// New query session. Save new json data to session.
//			echo "New query session. Save new json data to session.";
			$url = "http://localhost:8080/IR_Base/SearchQuery?search=".$search_keyword."&userId=".$userId;
			$ctx = stream_context_create(array('http'=>
			    array(
			        'timeout' => 120,  //120 Seconds is 2 Minutes
			    )
			));
			$json = file_get_contents($url,false,$ctx);
  			$_SESSION['search_keyword'] = $search_keyword;
			$_SESSION['jsonValue'] = $json;
		}

	//  Example hospital search result
	//	$url = "http://timan100.cs.uiuc.edu:8080/MedForums/MedForumSearch?query=".$search_keyword;

		
	//	echo $json;
		$json = mb_convert_encoding($json, 'UTF-8', 'UTF-8');
	//	echo $json;
		$obj = json_decode($json, true)["Reviews"];
	//	$obj = json_decode($json, true);		//original hospital form
	//	echo $obj;
		$length = count($obj);
		$numPage = ceil($length/$perPage);
		if($length>0){
			for ($index = $start; $index < $start+$perPage; $index++) {

				if ($index >= $length)
					break;

				$position = $index + 1;
				$json_attr = $obj[$index];
				$content = preg_replace('/((http|ftp|https):\/\/[\w-]+(\.[\w-]+)+([\w.,@?^=%&amp;:\/~+#-]*[\w@?^=%&amp;\/~+#-])?)/', '<a href="\1" target="_new">\1</a>', $json_attr['abstrct']);
				$author = $json_attr['authors'];
				$author_slash = addslashes($author);
				$author_array = explode(";",$author);
				if(sizeof($author_array)>5){		//Only display first 5 authors
					$author = "";
					for($i = 0; $i < 5; $i++){
						$author = $author."; ".$author_array[$i];
					}
					$author = substr($author, 1);
					$author = $author." et. al. ";
					$author_slash = addslashes($author);
				}
				$journalInfo = $json_attr['journalInfo'];
				if(isset($json_attr['journalInfo']) && strpos($json_attr['journalInfo'], "Published:") && strpos($json_attr['journalInfo'],"Volume:")){
					$journalName = explode("Volume:",$json_attr['journalInfo'])[0];
					$journalIssue = "Volume: ".explode("Published:",explode("Volume:",$json_attr['journalInfo'])[1])[0];
					if(strpos($journalIssue, "DOI:")){
						$doi = "DOI:".explode("DOI:",$journalIssue)[1];
					}
					else{
						$doi = "DOI:";
					}
				}
				elseif(isset($json_attr['journalInfo']) && strpos($json_attr['journalInfo'], "Published:") && strpos($json_attr['journalInfo'],"Pages:")){
					$journalName = explode("Pages:",$json_attr['journalInfo'])[0];
					$journalIssue = "Pages: ".explode("Published:",explode("Pages:",$json_attr['journalInfo'])[1])[0];
					if(strpos($journalIssue, "DOI:")){
						$doi = "DOI:".explode("DOI:",$journalIssue)[1];
					}
					else{
						$doi = "DOI:";
					}
				}
				else{
					$journalName = explode("Volume:",$json_attr['journalInfo'])[0];
					$doi = "DOI:";
					$journalIssue = "";
				}
				
				$date = explode("Published:",$json_attr['journalInfo'])[1];
				$docInfo = $json_attr['documentInfo'];
				$WOS_Num = $json_attr['wos'];//explode(" ",explode("WOS:",$docInfo)[1])[0];
				$title = $json_attr['title'];
				$title_slash = addslashes($title);
				$categories = $json_attr['categories'];
				$luceneIndex = $json_attr['docId'];
			//	$snippet = str_replace("****".$search_keyword."****","<mark>".$search_keyword."</mark>" ,$json_attr['snippet']);
				$snippet = preg_replace('/((http|ftp|https):\/\/[\w-]+(\.[\w-]+)+([\w.,@?^=%&amp;:\/~+#-]*[\w@?^=%&amp;\/~+#-])?)/', '<a href="\1" target="_new">\1</a>', $json_attr['snippet']);
				$abstract_highlight = preg_replace('/((http|ftp|https):\/\/[\w-]+(\.[\w-]+)+([\w.,@?^=%&amp;:\/~+#-]*[\w@?^=%&amp;\/~+#-])?)/', '<a href="\1" target="_new">\1</a>', $json_attr['abstrct_highlighted']);


			//	$content = preg_replace('/((http|ftp|https):\/\/[\w-]+(\.[\w-]+)+([\w.,@?^=%&amp;:\/~+#-]*[\w@?^=%&amp;\/~+#-])?)/', '<a href="\1" target="_new">\1</a>', $snippet);

				if ( 0) {
					$display_eval_form = "none";
					$display_eval_done = "block";
				} else {
					$display_eval_form = "block";
					$display_eval_done = "none";
				}
				echo "
				
				<div>
					<table class='search_results_table'>
						<tr>
							<td class='search_results_number'>$position</td>
							<td class='search_results_content col-lg-12'>
								<table>
									<tr><td class='search_results_title'>
										<h4>$title</h5>
									</td></tr>
									
									<tr><td>
										By: $author
									</td></tr>

									<tr><td>
										<B>$journalName</B> $journalIssue
									</td></tr>

									<tr><td>
										$date 
									</td></tr>
									<tr><td>
										Categories: $categories 
									</td></tr>
									<tr><td>
										<div class='h_tag_display'>
											<h5> $snippet </h5>
										</div>
									</td></tr>
			<!--					<tr><td>
										<div class='h_tag_display'>
											<h5> $abstract_highlight </h5>
										</div>
									</td></tr>
			-->	
									<tr><td>
										<table><tr>
											<td>
												<div id = 'snippet_$index' style='display: block'>
													<a role='button' class='btn-sm btn-primary abstract_button' href=\"javascript:read_more('snippet_$index', 'content_$index','contentButton_$index', '$WOS_Num', '$search_keyword','$index','$userId','$luceneIndex','$doi','$title_slash','$author_slash');\" id='document_$index'>Show Abstract</a>
												</div>
												<div id = 'contentButton_$index' style='display: none'>
													<a role='button' class='btn-sm btn-primary abstract_button' href=\"javascript:read_less('snippet_$index', 'content_$index','contentButton_$index', '$WOS_Num', '$search_keyword');\" id='document_$index'>Close Abstract</a>
												</div>
											</td>
											<td>
												<div>
													<a role='button' class='btn-sm btn-success' href=\"javascript:accessFullText('$WOS_Num', '$search_keyword','$index','$userId','$luceneIndex','$doi','$title_slash','$author_slash');\" id='search_$index'>Access Full Text</a>
												</div>
											</td>

										</tr></table>
									</td></tr>
									<tr><td>
										<div id = 'content_$index' style='display: none' class='h_abstract_display'>
											<b>Abstract</b><br>
											<h5>
												$abstract_highlight
											</h5>
										</div>
									</td></tr>

								</table>
							</td>
							<td class='search_results_data'>
								<b> </b>
							</td>

						</tr>			
					</table>
				 </div>";
				// array_push($displayed_docs, $WOS_Num);	//Set array of WOS of displayed doc
		//		array_push($clicked_docs, 0);			//Initialize array of WOS of clicked doc with 0s
	//			array_push($_SESSION['displayed_docs'], $WOS_Num);	//Set array of WOS of displayed doc
	//			array_push($_SESSION['clicked_docs'], 0);			//Initialize array of WOS of clicked doc with 0s
	//			echo count($_SESSION['displayed_docs']).'<br>';
	//			echo $_SESSION['displayed_docs'][0];

					

				array_push($displayed_docs, $WOS_Num);
				array_push($displayed_docs_lucene, $luceneIndex);
			}
			
			saveSearchLog($_SERVER['REMOTE_ADDR'], implode(",",$displayed_docs), $search_keyword, implode(",",$displayed_docs_lucene));
		}
		else{
			echo "<h4>No results found. New documents are now crawled. Please try again later.</h4>.";
		}

?>

		<div class="text-center">
			<nav aria-label="Page navigation">
			  <ul class="pagination">
			    <li>
			      <a href="#" aria-label="Previous">
			        <span aria-hidden="true">&laquo;</span>
			      </a>
			    </li>
			    <!-- <li class="active"><a href="#">1</a></li> -->
			    <?php
				    for ($index = 1; $index< $numPage+1; $index++) {
				    	if($index == $page){
				    		echo "<li class=\"active\"><a href=\"save_action.php?search_keyword=$search_keyword&page=$index\">$index</a></li>";
				    	}
				    	else{
				    		echo "<li><a href=\"save_action.php?search_keyword=$search_keyword&page=$index\">$index</a></li>";
				    	}
					}

			    ?>

			    <!-- 
			    <li><a href="#">2</a></li>
			    <li><a href="#">3</a></li>
			    <li><a href="#">4</a></li>
			    <li><a href="#">5</a></li> -->
			    <li>
			      <a href="#" aria-label="Next">
			        <span aria-hidden="true">&raquo;</span>
			      </a>
			    </li>
			  </ul>
			</nav>
		</div>